# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 11:20:23 2020

@author: au302246
"""

import pandas as pd
from datetime import datetime
import numpy as np
import os.path
import csv
import scipy.stats as sp
from time import mktime
import json
# read and process channel output file

def generate_performance(config_dict):
    
    performance_data_dict = {}

    for station in config_dict["stations"]:
        print(station["featureId"])
              
    
        # function start
        
        # Reading SWAT plus data:
        input_file = os.path.join(config_dict["paths"]["root_results"], config_dict["lookup"][station["featureId"]]["outputfile"])
        df_in = pd.read_csv(input_file, delimiter=r"\s+", header=1, skiprows=[2])
        df_in = df_in.rename(columns={"yr": "year", "mon": "month"})
        df_in["datetime"] = pd.to_datetime(df_in[['year', 'month', 'day']])
        
        # creating a query to get the right channel id:
        query_str = str(config_dict["lookup"][station["featureId"]]["idColumn"] + "==" + config_dict["lookup"][station["featureId"]]["ids"])
        selected_data = df_in.query(query_str)
        
        
        # for eact variable observed in the specified station, we pair obs and sim:
        for param in station["parameters"]:
            print(param["parameter"])
            
            # constructs the id as "datetime"
            SWAT_sim_df = selected_data[["datetime", param["parameter"]]].set_index("datetime", drop=True)
            
            # we convert precipitation to the correct unit mm
            SWAT_sim_precip = selected_data[["datetime", "area","precip"]].set_index("datetime", drop=True)
            SWAT_sim_precip["precip_mm"] = round((SWAT_sim_precip["precip"]/SWAT_sim_precip["area"]) * config_dict["precipScaleFactor"], 1)
            SWAT_sim_precip = SWAT_sim_precip.drop(columns=['area', 'precip'])
                       
            print("reading observations")    
            # Reading observation data    
            df_obs_in = pd.read_csv(os.path.join(config_dict["paths"]["root_obs"], station["featureId"]+"_"+param["parameter"]+".obs" ), delimiter="\t", header=0, skiprows=0)
            # make sure the format is correct for datetime before it is assigned to the index   
            df_obs_in["datetime"] = pd.to_datetime(df_obs_in["datetime"],format='%Y-%m-%d %H:%M:%S')
            df_obs = df_obs_in.set_index("datetime", drop=True)
        
            # we restrict the observations and the simulations to the specified datetime span
            datetimePasedFrom           = datetime.strptime(param["timeStart"], '%Y-%m-%d %H:%M:%S')
            datetimePasedEnd            = datetime.strptime(param["timeEnd"], '%Y-%m-%d %H:%M:%S')
            datetimePasedflagWarmupEnd  = datetime.strptime(param["flagWarmupEnd"], '%Y-%m-%d %H:%M:%S')
            
            df_obs = df_obs[(df_obs.index >= datetimePasedflagWarmupEnd) & (df_obs.index <=datetimePasedEnd )]
            SWAT_sim_df = SWAT_sim_df[(SWAT_sim_df.index >= datetimePasedFrom) & (SWAT_sim_df.index <= datetimePasedEnd)]
            SWAT_sim_precip = SWAT_sim_precip[(SWAT_sim_precip.index >= datetimePasedFrom) & (SWAT_sim_precip.index <= datetimePasedEnd)]
               
           
            # merge observations to nearest simulated value, thus using observations as superior, with a 4 hour threshold  
            df_sim_and_obs=pd.merge_asof(df_obs,SWAT_sim_df, left_index=True, right_index=True, direction = "nearest", tolerance=pd.Timedelta('4hours')).dropna()
        
            # we seperate in calibration and validation:
            datetimePasedflagCalibrationEnd  = datetime.strptime(param["flagCalibrationEnd"], '%Y-%m-%d %H:%M:%S')
            
            df_sim_and_obs_cal =  df_sim_and_obs[df_sim_and_obs.index <= datetimePasedflagCalibrationEnd]
            df_sim_and_obs_val = df_sim_and_obs[df_sim_and_obs.index > datetimePasedflagCalibrationEnd]
            
            # dividing paired values:
            
            time_cal = [ x for x in df_sim_and_obs.index if x < datetimePasedflagCalibrationEnd ]
            modelvals_cal = df_sim_and_obs[param["parameter"]][:len(time_cal)]
            modelvals_val = df_sim_and_obs[param["parameter"]][len(time_cal):]
            obsvals_cal = df_sim_and_obs["obs"][:len(time_cal)]
            obsvals_val = df_sim_and_obs["obs"][len(time_cal):]
                        
            # calculate statistics:
            s_cal, s_val = stat_init(param, df_sim_and_obs["obs"], df_sim_and_obs[param["parameter"]], df_sim_and_obs.index)
            
            sel_stats = ["type", "n", "NS", "BIAS", "RMSE", "RSQ"]    
            stat_matrix = []
            for key, value in s_cal.items():
                if key in sel_stats:
                    if param["separateCalVal"] == True:
                        stat_matrix.append([key,value,s_val.get(key)])
                    else:
                        stat_matrix.append([key,value])
            
            # getting Flow Duration Curve:
            data_sim, prob_sim, data_obs, prob_obs = flow_duration_curve(param, df_sim_and_obs)
            
            # getting flow distibution per month:
            distributions = flow_distributions(SWAT_sim_df, datetimePasedflagWarmupEnd, datetimePasedEnd )
            
           
            performance_data_dict[station["featureId"]] = {"precip" : { 
                                                                                "x": [t for t in SWAT_sim_precip.index.strftime("%Y-%m-%d %H:%M:%S")] ,
                                                                                "y": [t for t in SWAT_sim_precip["precip_mm"].values],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                           str(param["parameter"]+"$simulationCalibration") : { 
                                                                                "x": [t for t in df_sim_and_obs_cal.index.strftime("%Y-%m-%d %H:%M:%S")] ,
                                                                                "y": [t for t in df_sim_and_obs_cal[param["parameter"]].values],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                           str(param["parameter"]+"$simulationValidation") : { 
                                                                                "x": [t for t in df_sim_and_obs_val.index.strftime("%Y-%m-%d %H:%M:%S")] ,
                                                                                "y": [t for t in df_sim_and_obs_val[param["parameter"]].values],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                           str(param["parameter"]+"$observationsCalibration") : { 
                                                                                "x": [t for t in df_sim_and_obs_cal.index.strftime("%Y-%m-%d %H:%M:%S")] ,
                                                                                "y": [t for t in df_sim_and_obs_cal["obs"].values],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                           str(param["parameter"]+"$observationsValidation") : { 
                                                                                "x": [t for t in df_sim_and_obs_val.index.strftime("%Y-%m-%d %H:%M:%S")] ,
                                                                                "y": [t for t in df_sim_and_obs_val["obs"].values],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                           str(param["parameter"]+"$pairedValuesCalibration") : { 
                                                                                "x": [t for t in modelvals_cal.values] ,
                                                                                "y": [t for t in obsvals_cal.values],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                           str(param["parameter"]+"$pairedValuesValidation") : { 
                                                                                "x": [t for t in modelvals_val.values] ,
                                                                                "y": [t for t in obsvals_val.values],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                           str(param["parameter"]+"$simulationFdc") : { 
                                                                                "x": [t for t in prob_sim] ,
                                                                                "y": [t for t in data_sim],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                           str(param["parameter"]+"$observationsFdc") : { 
                                                                                "x": [t for t in prob_obs] ,
                                                                                "y": [t for t in data_obs],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                           str(param["parameter"]+"$simulationDistMonthQ05") : { 
                                                                                "x": [t for t in distributions["q05"].index] ,
                                                                                "y": [t for t in distributions["q05"][param["parameter"]]],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                           str(param["parameter"]+"$simulationDistMonthQ50") : { 
                                                                                "x": [t for t in distributions["q50"].index] ,
                                                                                "y": [t for t in distributions["q50"][param["parameter"]]],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                           str(param["parameter"]+"$simulationDistMonthQ95") : { 
                                                                                "x": [t for t in distributions["q95"].index] ,
                                                                                "y": [t for t in distributions["q95"][param["parameter"]]],
                                                                                "unit": "m<sup>3</sup>/s"
                                                                                },
                                                          str(param["parameter"]+"$statistics") : { 
                                                                                "x": [1] ,
                                                                                "y": [1] ,
                                                                                "cal": s_cal,
                                                                                "val": s_val,
                                                                                "unit": "na"
                                                                                }

                
                                                          }

    return performance_data_dict    
                
                

def flow_distributions(SWAT_sim_df, datetimePasedflagWarmupEnd, datetimePasedEnd ):
       
    SWAT_sim_for_distribution = SWAT_sim_df[(SWAT_sim_df.index >= datetimePasedflagWarmupEnd) & (SWAT_sim_df.index <= datetimePasedEnd)]
    
    SWAT_sim_for_distribution["m"] =  SWAT_sim_for_distribution.index.month
            
    distribution = {"q05":SWAT_sim_for_distribution.groupby(SWAT_sim_for_distribution["m"]).quantile(0.05),
                    "q50":SWAT_sim_for_distribution.groupby(SWAT_sim_for_distribution["m"]).quantile(0.50),
                    "q95":SWAT_sim_for_distribution.groupby(SWAT_sim_for_distribution["m"]).quantile(0.95)
                    }   
    return distribution





def flow_duration_curve(param, df):
    # https://pubs.usgs.gov/wsp/1542a/report.pdf
    # calculating the rank of simulations and observations:
    
    data_sim = df[param["parameter"]].dropna().values
    data_sim = np.sort(data_sim)
    ranks_sim = sp.rankdata(data_sim, method='average')
    ranks_sim = ranks_sim[::-1]
    prob_sim = [100*(ranks_sim[i]/(len(data_sim)+1)) for i in range(len(data_sim)) ]

    data_obs = df["obs"].dropna().values
    data_obs = np.sort(data_obs)
    ranks_obs = sp.rankdata(data_obs, method='average')
    ranks_obs = ranks_obs[::-1]
    prob_obs = [100*(ranks_obs[i]/(len(data_obs)+1)) for i in range(len(data_obs)) ]
    
    return data_sim, prob_sim, data_obs, prob_obs

# calculates statistics for a given period
def stat_init(param, obsvals, modelvals, time):
    
    if param["separateCalVal"] == True:
        end_calib_date = datetime.strptime(param["flagCalibrationEnd"], '%Y-%m-%d %H:%M:%S')
                
        include_val = True
        
        # creates a time seperator between cal and val for stat analysis
        time_cal = [ x for x in time if x < end_calib_date ]
        modelvals_cal = modelvals[:len(time_cal)]
        modelvals_val = modelvals[len(time_cal):]
        obsvals_cal = obsvals[:len(time_cal)]
        obsvals_val = obsvals[len(time_cal):]
               
    else:
        include_val = False
           
        modelvals_cal = modelvals
        obsvals_cal = obsvals
    
    stat_cal = statistics(obsvals_cal, modelvals_cal, type ="calibration")
    write_stats_to_screen(stat_cal)
        
    if include_val == True:
        stat_val = statistics(obsvals_val, modelvals_val, type = "validation")
        write_stats_to_screen(stat_val)
    else:
        stat_val = None
    return stat_cal, stat_val 


def write_stats_to_screen(stats):
        
    for k, v in stats.items():     
        print(str(str(k)+":"+str(v)))
        #self.dlg.textEdit_obs_statistics.append(str(str(k)+":"+str(v)))
    
    return
    
    
def statistics(obsvals, modelvals, type):    
    # calculate statistics for calibration and validation
    # calculate BIAS:
    if sum(obsvals) == 0:
        bias =-999
    else:
        bias = sum(((modelvals - obsvals)*100))/sum(obsvals)
    #print(bias)
    #calculate NS    
    ns = 1- (sum((obsvals - modelvals)**2)/ sum((obsvals - np.mean(obsvals))**2))                  
    
    n = len(obsvals)
    
    diff = modelvals-obsvals
    var_obs = ((obsvals-obsvals.mean())**2).mean()
    var_mod = ((modelvals-modelvals.mean())**2).mean()
    cov = ((obsvals-obsvals.mean())*(modelvals-modelvals.mean())).mean()
    cor = cov/np.sqrt(var_obs*var_mod)
    rsq = cor**2
    mae = np.mean(np.abs(diff))
    mape = np.mean(np.abs(diff/obsvals))*100
    mare = np.mean(np.abs(diff/obsvals))
    re = np.sum(np.abs(diff))/np.sum(obsvals)
    
    rmse = np.sqrt(np.mean(diff**2))
    
    # this one is active right now:
    # NS, BIAS, MAE, MAPE, RMSE, cor, RSQ, MARE, RE
    
    stat_dict = {   "type"  :type,
                    "n"     : n,
                    "NS"    : round(ns,2),
                    "BIAS"  : round(bias,1),
                    "MAE"   : round(mae,2),
                    "MAPE"  : round(mape,2),
                    "RMSE"  : round(rmse,2),
                    "cor"   : round(cor,2),
                    "RSQ"   : round(rsq,2),
                    "MARE"  : round(mare,2),
                    "RE"    : round(re,2)
                }
            
    #stats = [(round(ns,2)), (round(bias,1)), (round(mae,2)), (round(mape,2)), (round(rmse,2)), (round(cor,2)), (round(rsq,2)), (round(mare,2)), (round(re,2))]
        
    return stat_dict
































