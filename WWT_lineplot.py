

import plotly
import plotly.graph_objects as go
import os.path


 
def plotly_lineplot(dfForPlotting,plotpath):

    trace = []
    
    trace.append(go.Bar(
        x=dfForPlotting["precip"]["x"],
        y=dfForPlotting["precip"]["y"],
        yaxis = 'y2',
        xaxis = 'x',
        #fill='tonextx', fillcolor='rgba(0,100,80,0.2)',
        #mode= 'line',
        #visible = "legendonly",
        name = 'precip',
        marker_color='rgba(36, 149, 183, 0.88)'
        #line = dict(color = 'rgb(128, 128, 128)', width = 0)
        ))
    
       
    # creating the actual figure
    
    trace.append(go.Scatter(
        x=dfForPlotting["flo_out$observationsCalibration"]["x"],
        y=dfForPlotting["flo_out$observationsCalibration"]["y"],
        xaxis = 'x',
        yaxis = 'y',
        fill='tozeroy', fillcolor='rgba(0,100,80,0.3)',
        #mode= "markers",
        #marker_color='rgba(36, 149, 183, 0.88)',
        #visible = "legendonly",
        name = "observations (cal)",
        line = dict(color = 'rgb(128, 128, 128)', width = 0)
        ))
       
    trace.append(go.Scatter(
        x=dfForPlotting["flo_out$observationsValidation"]["x"],
        y=dfForPlotting["flo_out$observationsValidation"]["y"],
        xaxis = 'x',
        yaxis = 'y',
        fill='tozeroy', fillcolor='rgba(0,100,80,0.2)',
        #mode= 'markers',
        name = "observations (val)",
        visible = "legendonly",
        line = dict(color = 'rgb(128, 128, 128)', width = 0)
        ))    
    
    trace.append(go.Scatter(
        x=dfForPlotting["flo_out$simulationCalibration"]["x"],
        y=dfForPlotting["flo_out$simulationCalibration"]["y"],
        xaxis = 'x',
        yaxis = 'y',
        #fill='tonexty', fillcolor='rgba(0,100,80,0.2)',
        #mode= 'markers',
        name = "simulation (cal)",
        #visible = "legendonly",
        line = dict(color = 'rgb(128, 128, 128)', width = 2)
        ))
        
    trace.append(go.Scatter(
        x=dfForPlotting["flo_out$simulationValidation"]["x"],
        y=dfForPlotting["flo_out$simulationValidation"]["y"],
        xaxis = 'x',
        yaxis = 'y',
        #fill='tonexty', fillcolor='rgba(0,100,80,0.2)',
        #mode= 'markers',
        name = "simulation (val)",
        visible = "legendonly",
        line = dict(color = 'rgb(128, 128, 110)', width = 2)
        ))
    
    
    layout = dict(
            hovermode="closest",
            yaxis = dict(
                    hoverformat = '.2f',
                    title=str("discharge (m3/s)"),
                    showgrid = False,
                    linecolor="grey",
                    domain=[0, 0.70],
                    #range = [0,5000]
                    ),
            
            yaxis2=dict(
                    domain=[0.75, 1],
                    range = [max(dfForPlotting["precip"]["y"]), 0],
                    hoverformat = '.2f',
                    title=str("precip (mm)"),
                    showgrid = False,
                    linecolor="grey",
                    ),
                  
                                     
            legend = dict(orientation="h", x = 0.0, y = 1.1),
            
            margin = dict(
                    l = 50,
                    r = 50,
                    b = 50,
                    t = 50,
                    pad = 4
                  ),
                
            plot_bgcolor="white",#'rgba(207, 207, 206, 0.7)'
            #autosize=False,
            #width=790,
            #height=500,
            
            #title= str("(generated at: " +dt_now_str + ")"),
            
            #xref="paper",
            #x=1,
            #title= str("<b>ASAP Reservoir water forecast</b>" + " \n (generated at: " +dt_now_str + ")<br>" + "location: " + location + "</b>"),
            font=dict(size=10),
                    
            xaxis = dict(title=' ', #tickformat ="<b>%a </b> \n %d-%b \n",
                             #dtick = 86400000.0,
                             #overlaying = 'x', 
                             side = 'bottom',
                             #anchor = 'free',
                             #gridcolor = "white",
                             #gridwidth = 4,
                             #ticks='outside',
                             #position= 0.99,
                             #range = ["2021-04-01","2023-10-10"]
                             #range = [dt_offset_str,dt_forecast_max]
                             ),
            
            )
            
    #py.iplot(data, filename='basic-area-no-bound')
    
    
    fig = go.Figure(data=trace, layout = layout)
    plotly.offline.plot(fig, auto_open=True, filename=os.path.join(plotpath,"lineplot.html"))

    return