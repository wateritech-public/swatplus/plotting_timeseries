# SWAT plus time series plotting

We're sharing this script to assist time series plotting of a SWAT plus model simulation.
We hope that the workflow may ease SWAT model applications. Reach out if you have questions.

## How to:
* Unzip the example [data output](./ExampleData/vejle/SWATplus.zip) included in this repository at its location 
* Configure the [config_stations.json](./config_stations.json) with paths and temporal splitting in warmup, calibration and validation periods and also number of stations to include.
* Observation data is located in the folder [observations](./ExampleData/vejle/observations/station1_flo_out.obs)
* Run the [01_MainPerformance.py](./01_MainPerformance.py) in e.g. spyder
* Outputs are located in this [folder](./ExampleData/vejle/out)
* Once the example has been succesfully run you can change the SWATplus simulation output file and the observations with your own data

## Outputs:

Outputs from the script are time series and performance statistics exported to a json file:

<img src="./Illustrations/Output.png" alt="image" width="20%" height="auto">

And the plot below.

![Output plot as html](./Illustrations/plot.png)

## Hints:

NB between versions of SWAT plus you may need to adjust the precipitation factor in the configurtion file to get the right conversion to mm
```json
"precipScaleFactor" : 1000,
```

When using your own data you ahve to modify the GIS ID of the stream you wish to pair with observations:  
```json
"lookup": {"station1": {"outputfile"  : "channel_sd_day.txt",
                       "idColumn"     : "gis_id",
                       "ids"          : "34"
                       },
           "station2": {"outputfile"  : "channel_sd_day.txt",
                       "idColumn"     : "gis_id",
                       "ids"          : "13"
                       }
         }
```

Configuration of the warmup, calibration and validation periods are set here (in the config json file):
```json
"stations": [
    {"landscape": "stations",
    "featureId": "station1",
    "parameters": [{"parameter":          "flo_out",
                    "timeStart":           "2014-08-01 00:00:00", 
                    "timeEnd":             "2018-07-31 00:00:00",
                    "flagWarmupEnd":       "2014-08-01 00:00:00",
                    "flagCalibrationEnd":  "2016-07-31 00:00:00",
                    "separateCalVal":      true
                   }
                   ]
     } 
```




