# -*- coding: utf-8 -*-

import os.path
import json
import UTILS_SWATplus_performance as utilperform
import WWT_lineplot

config_file = "./config_stations.json"
plotpath = "./ExampleData/vejle/out"

with open(config_file, 'r') as f:
    config_dict = json.load(f)

performDict = utilperform.generate_performance(config_dict)

with open(os.path.join(config_dict["paths"]["root_out"],"stations.json"), 'w') as outfile:
    json.dump(performDict, outfile, indent = 4, ensure_ascii = False)

WWT_lineplot.plotly_lineplot(performDict["station1"],plotpath)





















